import cv2
import os
import os.path

PATH_TO_IMAGES = '/media/lukas/Useful Programs/RGBD datasets/POVID+DP/SUNRGBD/kv2/kinect2data/'


def show_images(work):
    if not work: return
    if not os.path.exists(PATH_TO_IMAGES):
        return
    folders = os.listdir(PATH_TO_IMAGES)
    if len(folders) == 0:
        return
    for folder in folders:
        if not os.path.isdir(PATH_TO_IMAGES + folder ):
            continue
        image_path = os.listdir(PATH_TO_IMAGES + folder + '/image/')
        file_name = PATH_TO_IMAGES + folder + '/image/' + image_path[0]
        depth_path = os.listdir(PATH_TO_IMAGES + folder + '/depth/')
        depth_name = PATH_TO_IMAGES + folder + '/depth/' + depth_path[0]
        image = cv2.imread(file_name)
        depth = cv2.imread(depth_name)
        cv2.imshow(folder, image)
        cv2.imshow('depth', depth)
        key = cv2.waitKey()
        if key == ord('q'):
            exit()
        else:
            cv2.destroyWindow(folder)
