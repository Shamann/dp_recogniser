import cv2
import numpy as np
import csv
import math
import os


class Superpixel(object):
    def __init__(self):
        self.id = -1
        self.border = False
        self.features = []
        self.neighbors = set()
        self.segment = []
        self.classified = False
        self.labels = []
        self.class_s = []
        self.class_r = []

    def bounding_rect(self, ds=0):
        x, y, w, h = cv2.boundingRect(self.segment)
        dims = self.segment.shape
        w = dims[1] if w + 2 * ds > dims[1] else w + 2 * ds
        h = dims[0] if h + 2 * ds > dims[0] else h + 2 * ds
        x = 0 if x - ds < 0 else x - ds
        y = 0 if y - ds < 0 else y - ds
        if x == 0:
            w -= ds
        if y == 0:
            h -= ds
        return x, y, w, h

    def cosine_similarity(self, superpixel_2):
        dot = 0.
        denom_a = 0.
        denom_b = 0.
        a = self.features[:]
        b = superpixel_2.features[:]
        k = lcm(len(a), len(b))
        k_a = int(k / len(a))
        k_b = int(k / len(b))
        for i in range(1, k_a):
            a += self.features[:]
        for i in range(1, k_b):
            b += superpixel_2.features[:]
        for i in range(0, len(a)):
            dot += a[i] * b[i]
            denom_a += a[i] * a[i]
            denom_b += b[i] * b[i]
        return dot / (math.sqrt(denom_a) * math.sqrt(denom_b))

    def merge(self, superpixel_2):
        # merge if the superpixel is on border
        if superpixel_2.border:
            self.border = True
        # merge features
        # compute average
        # for i in range(0,7):
            # self.features[i] = (self.features[i] + superpixel_2.features[i]) / 2
        # add all together
        self.features += superpixel_2.features
        # merge neighbors
        self.neighbors |= superpixel_2.neighbors
        try:
            self.neighbors.remove(self.id)
        except:
            pass
        self.neighbors.remove(superpixel_2.id)
        # merge segment masks
        self.segment = cv2.bitwise_or(self.segment, superpixel_2.segment)

    def centroid(self):
        im2, contours, hierarchy = cv2.findContours(self.segment, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        m = cv2.moments(contours[0])
        c_x = int(m["m10"] / m["m00"])
        c_y = int(m["m01"] / m["m00"])
        return [c_x, c_y]

    def fill_holes(self):
        im2, contours, hierarchy = cv2.findContours(self.segment, 1, 2)
        for i in range(0, len(contours)):
            cv2.drawContours(self.segment, contours, i, [255], cv2.FILLED)
        return


def load_superpixels(image_path, image_name):
    image_mat = cv2.imread(image_path + '/' + image_name)
    depth_mat = cv2.imread(os.path.dirname(image_path) + '/depth/' + os.path.splitext(image_name)[0] + '.png',
                           cv2.IMREAD_UNCHANGED)
    minmax_depth = cv2.minMaxLoc(depth_mat)
    indexes = cv2.imread(image_path + '/superpixels.png', -1)
    superpixels = {}
    features = open(image_path + '/features.txt')
    neighbors = open(image_path + '/neighborhood.csv')
    csvreader = csv.reader(neighbors, delimiter=';')
    shape = [image_mat.shape[0], image_mat.shape[1], 1]
    for line in features:
        data = line.split(' ')
        sup = Superpixel()
        sup.id = int(data[0])
        sup.border = bool(data[1])
        for i in range(2, 5):
            sup.features.append(float(data[i]) / 255)
        sup.features.append(float(data[5]) / minmax_depth[1])
        for i in range(6, len(data)):
            sup.features.append(float(data[i]))
        row = next(csvreader)
        while not int(row[0]) == sup.id:
            row = next(csvreader)
        for i in range(1, len(row)):
            sup.neighbors.add(int(row[i]))
        sup.segment = np.zeros(shape, dtype="uint8")
        superpixels[sup.id] = sup
    height, width = indexes.shape
    for row in range(0, height):
        for col in range(0, width):
            idx = indexes[row, col]
            if not idx == 0:
                superpixels[idx].segment[row, col] = 255
    return superpixels, image_mat, indexes


def gcd(x, y):
    """This function implements the Euclidian algorithm
    to find G.C.D. of two numbers"""
    while y:
        x, y = y, x % y
    return x


# define lcm function
def lcm(x, y):
    """This function takes two
    integers and returns the L.C.M."""
    lcm_n = (x * y) // gcd(x, y)
    return lcm_n

def global_similarity(superpixels, part=0.25):
    size = len(superpixels)
    s_p_matrix = []
    keys = list(superpixels.keys())
    for x in range(size):
        for y in range(x+1,size):
            s_p_matrix.append(superpixels[keys[x]].cosine_similarity(superpixels[keys[y]]))

    s_p_matrix = sorted(s_p_matrix, reverse=True)


    glob_sim = s_p_matrix[int(len(s_p_matrix)*part)]

    return glob_sim