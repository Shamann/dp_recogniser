""" Master thesis iterative classification based segment merging method.

The image classification is run with Inception trained on ImageNet 2012 Challenge dataset.

https://tensorflow.org/tutorials/image_recognition/
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from numpy.ma.core import diff

from show_images import *
from superpixel import *
from six.moves import urllib

import argparse
import os
import os.path
import datetime
import re
import sys
import tarfile
import cv2
import copy

import numpy as np
import multiprocessing as mp
import collections as cl
import tensorflow as tf

TEST_IMAGE = 'TestImages/0000103.jpg'
DESKTOP_SAMPLE = '/home/lukas/Desktop/Selection/'

FLAGS = None

# pylint: disable=line-too-long
DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'
IMAGENET_PATH = 'imagenet/'
IMAGENET_PBTXT = 'imagenet_2012_challenge_label_map_proto.pbtxt'
IMAGENET_TXT = 'imagenet_synset_to_human_label_map.txt'
IMAGE_GRAPH_DEF_PB = 'classify_image_graph_def.pb'


# pylint: enable=line-too-long

class ImageLookup(object):
    def __init__(self, base_path):
        self.m_folder_index = 0
        self.base_path = base_path
        self.folders_list = os.listdir(base_path)

    def next_file_folder(self):
        path = self.base_path + self.folders_list[self.m_folder_index] + "/image/"
        if not os.path.exists(path):
            return []
        images = []
        files = os.listdir(path)
        files = sorted(files, key=str.lower)
        for filename in files:
            fl, extension = os.path.splitext(filename);
            if extension == '.jpg':
                images.append(path + filename)
        self.m_folder_index += 1
        return images


class NodeLookup(object):
    """Converts integer node ID's to human readable labels."""

    def __init__(self,
                 label_lookup_path=None,
                 uid_lookup_path=None):
        if not label_lookup_path:
            label_lookup_path = os.path.join(
                FLAGS.model_dir, IMAGENET_PBTXT)
        if not uid_lookup_path:
            uid_lookup_path = os.path.join(
                FLAGS.model_dir, IMAGENET_TXT)
        self.node_lookup = self.load(label_lookup_path, uid_lookup_path)

    def load(self, label_lookup_path, uid_lookup_path):
        """Loads a human readable English name for each softmax node.

        Args:
          label_lookup_path: string UID to integer node ID.
          uid_lookup_path: string UID to human-readable string.

        Returns:
          dict from integer node ID to human-readable string.
        """
        if not tf.gfile.Exists(uid_lookup_path):
            tf.logging.fatal('File does not exist %s', uid_lookup_path)
        if not tf.gfile.Exists(label_lookup_path):
            tf.logging.fatal('File does not exist %s', label_lookup_path)

        # Loads mapping from string UID to human-readable string
        proto_as_ascii_lines = tf.gfile.GFile(uid_lookup_path).readlines()
        uid_to_human = {}
        p = re.compile(r'[n\d]*[ \S,]*')
        for line in proto_as_ascii_lines:
            parsed_items = p.findall(line)
            uid = parsed_items[0]
            human_string = parsed_items[2]
            uid_to_human[uid] = human_string

        # Loads mapping from string UID to integer node ID.
        node_id_to_uid = {}
        proto_as_ascii = tf.gfile.GFile(label_lookup_path).readlines()
        for line in proto_as_ascii:
            if line.startswith('  target_class:'):
                target_class = int(line.split(': ')[1])
            if line.startswith('  target_class_string:'):
                target_class_string = line.split(': ')[1]
                node_id_to_uid[target_class] = target_class_string[1:-2]

        # Loads the final mapping of integer node ID to human-readable string
        node_id_to_name = {}
        for key, val in node_id_to_uid.items():
            if val not in uid_to_human:
                tf.logging.fatal('Failed to locate: %s', val)
            name = uid_to_human[val]
            node_id_to_name[key] = name

        return node_id_to_name

    def id_to_string(self, node_id):
        if node_id not in self.node_lookup:
            return ''
        return self.node_lookup[node_id]


def create_graph():
    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    with tf.gfile.FastGFile(os.path.join(
            FLAGS.model_dir, IMAGE_GRAPH_DEF_PB), 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')


def run_classification(image, k=1):
    """
    :param image: Image in numpy array
    :return: classification top1 score
    """
    with tf.Session() as sess:
        softmax_sensor = sess.graph.get_tensor_by_name('softmax:0')
        predictions = sess.run(softmax_sensor, {'DecodeJpeg:0': image})
        predictions = np.squeeze(predictions)
        node_lookup = NodeLookup()
        top_k = predictions.argsort()[-k:][::-1]
        out = []
        for node_id in top_k:
            label = node_lookup.id_to_string(node_id)
            score = predictions[node_id]
            out.append([label, score])
        return out


def run_inference_on_image(image):
    """Runs inference on an image.

    Args:
      image: Image file name.

    Returns:
      Nothing
    """
    if not tf.gfile.Exists(image):
        tf.logging.fatal('File does not exist %s', image)
    # image_data = tf.image.decode_png(image)
    image_data = tf.gfile.FastGFile(image, 'rb').read()

    # Creates graph from saved GraphDef.
    # create_graph()

    print('\nClassification of file: %s' % image)
    with tf.Session() as sess:
        # Some useful tensors:
        # 'softmax:0': A tensor containing the normalized prediction across
        #   1000 labels.
        # 'pool_3:0': A tensor containing the next-to-last layer containing 2048
        #   float description of the image.
        # 'DecodeJpeg/contents:0': A tensor containing a string providing JPEG
        #   encoding of the image.
        # Runs the softmax tensor by feeding the image_data as input to the graph.
        softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
        predictions = sess.run(softmax_tensor,
                               {'DecodeJpeg/contents:0': image_data})
        predictions = np.squeeze(predictions)

        # Creates node ID --> English string lookup.
        node_lookup = NodeLookup()

        top_k = predictions.argsort()[-FLAGS.num_top_predictions:][::-1]
        out = []
        for node_id in top_k:
            human_string = node_lookup.id_to_string(node_id)
            score = predictions[node_id]
            print('%s (score = %.5f)' % (human_string, score))
            out.append([human_string, score])
        return out


def maybe_download_and_extract():
    """Download and extract model tar file."""
    dest_directory = FLAGS.model_dir
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = DATA_URL.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (
                filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Succesfully downloaded', filename, statinfo.st_size, 'bytes.')
    tarfile.open(filepath, 'r:gz').extractall(dest_directory)


def maybe_in_root_folder():
    parent_directory = os.path.dirname(os.getcwd())
    imagenet_absolute_path = os.path.join(parent_directory, IMAGENET_PATH)
    imagenet_path_txt = os.path.join(imagenet_absolute_path, IMAGENET_TXT)
    imagenet_path_pbtxt = os.path.join(imagenet_absolute_path, IMAGENET_PBTXT)
    imagenet_path_graph = os.path.join(imagenet_absolute_path, IMAGE_GRAPH_DEF_PB)
    if os.path.exists(imagenet_absolute_path):
        if os.path.exists(imagenet_path_txt):
            if os.path.exists(imagenet_path_pbtxt):
                if os.path.exists(imagenet_path_graph):
                    FLAGS.model_dir = imagenet_absolute_path
                    return True
    return False


def find_jpeg_in_dir(dir_name):
    files = os.listdir(dir_name)
    for file in files:
        nm, ext = os.path.splitext(file)
        ext = ext.lower()
        if ext == '.jpg' or ext == '.jpeg':
            return file


def load_classification(classification_csv):
    csvreader = csv.reader(open(classification_csv), delimiter=';')
    classification = []
    for row in csvreader:
        field = []
        for i in range(0, len(row)):
            field.append(float(row[i]) if i < len(row) - 2 else row[i])
        classification.append(field)
    return classification


def get_field_from_scores(scores, k):
    labels = []
    for s in scores:
        labels.append(s[k])
    return labels


def classify_segment(value, original_image, ds, ks=1, kr=1, roi=True, bcg=False):
    x, y, w, h = value.bounding_rect(ds)
    image_segment = cv2.bitwise_and(original_image, (cv2.cvtColor(value.segment, cv2.COLOR_GRAY2BGR)))

    # if bcg:
    #     background = np.zeros(cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY).shape) + 110
    #     image_mask = cv2.inRange(image_segment, 0, 1)
    #     image_segment = cv2.bitwise_or(cv2.bitwise_and(background, image_mask), image_segment)

    image_segment = image_segment[y:y + h, x:x + w]
    image_roi = original_image[y:y + h, x:x + w]
    # cv2.imshow('segment',image_segment)
    # cv2.imshow('roi',image_roi)
    # cv2.waitKey(0)

    results_segment = run_classification(image_segment, ks)
    results_roi = run_classification(image_roi, kr)
    if roi:
        return results_segment, results_roi
    else:
        return results_segment


def classify_segment_roi(value, original_image, ds=5, k=3):
    rois = []
    for i in range(k):
        x, y, w, h = value.bounding_rect(i * ds)
        image_roi = original_image[y:y + h, x:x + w]
        rois.append(run_classification(image_roi, 1)[0])
    same = 0
    segm_fault = 0
    new = 0
    strange = 0
    for i in range(1, len(rois)):
        if rois[0][0] == rois[i][0] and rois[0][1] < rois[i][1]:
            same += 1
        if rois[0][0] != rois[i][0] and rois[0][1] > rois[i][1]:
            segm_fault += 1
        if rois[0][0] != rois[i][0] and rois[0][1] < rois[i][1]:
                new += 1
                new_idx = i
        if rois[i - 1][0] != rois[i][0] and rois[i - 1][1] < rois[i][1]:
            strange += 1
    if same >= int((k-1) / 2):
        return rois[0]
    if segm_fault > 1:
        if new > 1:
            return rois[new_idx]
        else:
            rois = sorted(rois[1:len(rois)], key=lambda x: x[1])
            return rois[0]
    if strange > 1:
        return rois[0]
    rois = sorted(rois, key=lambda x: x[1])
    return rois[0]

def classify_image(image_path):
    # try:
#    image_path = DESKTOP_SAMPLE + image_base_path
    print('%s\n' % image_path)
    classification_csv = image_path + '/classification.csv'
    image_name = find_jpeg_in_dir(image_path + '/')

    superpixels, original_image, original_segments = load_superpixels(image_path, image_name)

    ds = 15
    i = 0

#    result = run_classification(original_image, 10)
#    for r in result:
#        print('label: %s  score: %0.7lf' % (r[0], r[1]))

    scores = []  # [0] score of segment [1] score of bounding rectangle [2] diff
    if os.path.exists(classification_csv):
        scores = load_classification(classification_csv)
    else:
        f = open(classification_csv, 'w')
        for key, value in superpixels.items():
            if not value.border:
                continue
            results_segment, results_roi = classify_segment(value, original_image, ds)
            scores.append(
                [value.id, results_segment[0][1], results_roi[0][1], abs(results_roi[0][1] - results_segment[0][1]),
                 results_segment[0][0], results_roi[0][0]])
            f.write('%d; %.7lf; %.7lf; %.7lf; %s; %s' % (
                value.id, results_segment[0][1], results_roi[0][1], abs(results_roi[0][1] - results_segment[0][1]),
                results_segment[0][0], results_roi[0][0]))
            f.write('\n')
            i += 1
#            print('Segment %d classified... [%.2lf]' % (value.id, i * 100 / len(superpixels)))
        f.close()
    scores = sorted(scores, key=lambda scores: scores[5])
    labels = get_field_from_scores(scores, 5)
    freqs = cl.Counter(labels).most_common()

    similarity_thresh = global_similarity(superpixels, 0.12)
    max_iter = 3
    similarity_step = (1 - similarity_thresh) / max_iter

    for f in freqs:
        most_predicted = list(filter(lambda x: x[5] == f[0], scores))
        most_predicted = sorted(most_predicted, key=lambda x: -cv2.countNonZero(superpixels[x[0]].segment))

        for m in most_predicted:
            # centroid = ftools.reduce(lambda x, y: [x[0] + y[0], x[1] + y[1]], centroids)
            # centroid[0] = int(centroid[0] / len(centroids))
            # centroid[1] = int(centroid[1] / len(centroids))
            # object_seed = original_segments[centroid[1], centroid[0]]
            try:
                object_seed = superpixels[m[0]]
            except:
                continue

            iteration = 1
            while iteration < max_iter:
    #            print('ID: %d Iteration: %d' % (object_seed.id, iteration))
                neighbors = object_seed.neighbors.copy()
                for n_id in neighbors:
                    try:
                        n = superpixels[n_id]
                        # if n.classified:
                        #     continue
                    except:
                        object_seed.neighbors.remove(n_id)
                        continue
                    similarity = object_seed.cosine_similarity(n)
#                    print('  N: %d similarity: %.7lf' % (n.id, similarity))
                    if similarity > 1 - similarity_step * iteration:
#                        print('  merged')
                        object_seed.merge(n)
                        superpixels.pop(n_id)
                        for ns in n.neighbors:
                            if not ns == object_seed.id:
                                try:
                                    superpixels[ns].neighbors.remove(n_id)
                                except:
                                    pass
                                try:
                                    superpixels[ns].neighbors.add(object_seed.id)
                                except:
                                    pass
                        try:
                            most_predicted.pop(n_id)
                        except:
                            pass
                        try:
                            for s in scores:
                                if s[0] == n_id:
                                    scores.remove(s)
                                    break
                        except:
                            pass
                iteration += 1

    if not os.path.exists(image_path + '/seeds'):
        os.mkdir(image_path + '/seeds')
    for key, val in superpixels.items():
        cv2.imwrite(image_path + '/seeds/seed_' + str(val.id) + '.png',
                    cv2.bitwise_and((cv2.cvtColor(val.segment, cv2.COLOR_GRAY2BGR)), original_image))

    ds = 8
    for key, val in superpixels.items():
        superpixels[key].class_s, superpixels[key].class_r = classify_segment(superpixels[key], original_image, ds,
                                                                              bcg=True)
        superpixels[key].class_s = superpixels[key].class_s[0]
        superpixels[key].class_r = superpixels[key].class_r[0]
#        print('k_s %d: %s %.7lf' % (key, superpixels[key].class_s[0], superpixels[key].class_s[1]))
#        print('k_r %d: %s %.7lf' % (key, superpixels[key].class_r[0], superpixels[key].class_r[1]))

    converged = False
    while not converged:
        keys = sorted(list(superpixels), key=lambda k: len(superpixels[k].neighbors))
        non_mergeds = 0
        for key in keys:
            try:
                object_seed = superpixels[key]
            except:
                continue
            neighbors = object_seed.neighbors.copy()

            while True:
                # classify segment (object seed)
                seed_classify_s = object_seed.class_s
                seed_classify_r = object_seed.class_r
#                print('k_s %d: %s %.7lf' % (key, seed_classify_s[0], seed_classify_s[1]))
#                print('k_r %d: %s %.7lf' % (key, seed_classify_r[0], seed_classify_r[1]))

                if seed_classify_s[0] != seed_classify_r[0] and seed_classify_s[1] > seed_classify_r[1]:
                    seed_classify = seed_classify_r[:]
                else:
                    seed_classify = seed_classify_s[:]

                mergeds_classify = {}
                # for each of your neighbors
                for n in list(neighbors):
                    # do something only if segments are at least 70% similar
                    try:
                        superpixels[n]
                    except KeyError:
                        neighbors.remove(n)
                        continue

                    similarity = superpixels[key].cosine_similarity(superpixels[n])
                    if similarity < 0.75:
                        continue
                    # deep copy of seed and neighbor
                    object_seed_cp = copy.deepcopy(superpixels[key])
                    n_x = copy.deepcopy(superpixels[n])

                    # classify neighbor before merge
                    neighbor_classify_s = n_x.class_s
                    neighbor_classify_r = n_x.class_r
                    if neighbor_classify_s[0] != neighbor_classify_r[0] and neighbor_classify_s[1] > \
                            neighbor_classify_r[1]:
                        neighbor_classify = neighbor_classify_r[:]
                    else:
                        neighbor_classify = neighbor_classify_s[:]
                    # temporary merge seed + neighbor
                    object_seed_cp.merge(n_x)

                    # classify merged
                    merged_classify_s, merged_classify_r = classify_segment(object_seed_cp, original_image, ds,
                                                                            bcg=True)
                    merged_classify_s = merged_classify_s[0]
                    merged_classify_r = merged_classify_r[0]
                    if merged_classify_s[0] != merged_classify_r[0] and merged_classify_s[1] > merged_classify_r[1]:
                        merged_classify = merged_classify_r[:]
                    else:
                        merged_classify = merged_classify_s[:]
#                    print('  n %d: %s %.7lf' % (n, neighbor_classify[0], neighbor_classify[1]))
#                    print('  m %d: %s %.7lf' % (n, merged_classify[0], merged_classify[1]))

                    # compare classification results
                    # TODO Verify comparison to neighbor segment classification
                    # and (merged_classify[0] == seed_classify[0] or merged_classify[0] == neighbor_classify[0])
                    if (merged_classify[1] - seed_classify[1] > 0.02 and merged_classify[1] - neighbor_classify[1] > 0.02) \
                            or similarity > similarity_thresh:
                        mergeds_classify[n] = merged_classify
#                        print('    added %d' % n)
                if mergeds_classify == {}:
                    non_mergeds += 1
                    break

                best_fit_merge_key = sorted(list(mergeds_classify), key=lambda k: -mergeds_classify[k][1])
                object_seed.merge(superpixels[best_fit_merge_key[0]])
#                print("Merged: %d with %d" % (object_seed.id, best_fit_merge_key[0]))
                object_seed.class_s = mergeds_classify[best_fit_merge_key[0]]
                object_seed.class_r = classify_segment_roi(object_seed, original_image, ds, k=1)
                best_fit_merge_value = superpixels.pop(best_fit_merge_key[0])
                for ns in best_fit_merge_value.neighbors:
                    if not ns == object_seed.id:
                        try:
                            superpixels[ns].neighbors.remove(best_fit_merge_key[0])
                        except:
                            pass
                        try:
                            superpixels[ns].neighbors.add(object_seed.id)
                        except:
                            pass
                        try:
                            neighbors.remove(ns)
                        except:
                            pass
                if n == list(neighbors)[-1]:
                    break
            if non_mergeds == len(superpixels):
                converged = True
    # except Exception:
    #     print("Unexpected error: %s" % sys.exc_info()[0])
    #     continue
    for key in list(superpixels):
        superpixels[key].labels = classify_segment(superpixels[key], original_image, ds, roi=False, ks=5)
        # superpixels[key].labels = superpixels[key].labels[0]
        superpixels[key].classified = True

    if not os.path.exists(image_path + '/segments'):
        os.mkdir(image_path + '/segments')
    l_f = open(image_path + '/segments/labels.txt', 'w')
    for key, s in superpixels.items():
        s = superpixels[key]
        image_segment = cv2.bitwise_and(original_image, (cv2.cvtColor(s.segment, cv2.COLOR_GRAY2BGR)))

        cv2.imwrite(image_path + '/segments/' + str(s.id) + '_s.png', image_segment)
        lp_f = open(image_path + '/segments/' + str(s.id) + '_l.txt', 'w')
        if s.classified:
            first = True
            for label in s.labels:
                if first:
                    print(label)
                    l_f.write('%d -> %s %.9lf\n' % (s.id, label[0], label[1]))
                    first = False
                lp_f.write('%d -> %s %.9lf\n' % (s.id, label[0], label[1]))
        lp_f.close()
    l_f.close()

def main(_):
    show_images(False)

    if not maybe_in_root_folder():
        maybe_download_and_extract()
    create_graph()

    im_paths = os.listdir(DESKTOP_SAMPLE + 'Evaluation/')
    for i in range(len(im_paths)):
        im_paths[i] = DESKTOP_SAMPLE + 'Evaluation/' + im_paths[i] + '/image'

    #im_paths = ['Objects/5/image', 'Objects/3/image', 'Objects/1/image']

    print('start: %s' % datetime.datetime.now())

    processes = mp.cpu_count() - 1
    pool = mp.Pool(processes=processes)
    pool.map(classify_image, im_paths)

    print('end: %s' % datetime.datetime.now())
        # image_path = os.path.join(os.path.dirname(os.path.abspath(FLAGS.model_dir)), TEST_IMAGE)
        # image = (image_path if os.path.exists(image_path) else
        #          os.path.join(FLAGS.model_dir, 'cropped_panda.jpg'))
        # manager = ImageLookup(DESKTOP_SAMPLE)
        # number_of_samples = len(os.listdir(DESKTOP_SAMPLE))
        # for x in range(0,number_of_samples):
        #     folder = manager.next_file_folder()
        #     if not folder:
        #         break
        #     else:
        #         result_file = folder[0]
        #         result_file = os.path.splitext(result_file)[0] + '.csv'
        #         f = open(result_file, 'w')
        #         for image in folder:
        #             results = run_inference_on_image(image)
        #             for r in results:
        #                 f.write('%s;%s (score = %.5f);' % (os.path.basename(image), r[0], r[1]))
        #                 f.write('\n')
        #         f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # classify_image_graph_def.pb:
    #   Binary representation of the GraphDef protocol buffer.
    # imagenet_synset_to_human_label_map.txt:
    #   Map from synset ID to a human readable string.
    # imagenet_2012_challenge_label_map_proto.pbtxt:
    #   Text representation of a protocol buffer mapping a label to synset ID.
    parser.add_argument(
        '--model_dir',
        type=str,
        default='/tmp/imagenet',
        help="""\
      Path to classify_image_graph_def.pb,
      imagenet_synset_to_human_label_map.txt, and
      imagenet_2012_challenge_label_map_proto.pbtxt.\
      """
    )
    parser.add_argument(
        '--image_file',
        type=str,
        default='',
        help='Absolute path to image file.'
    )
    parser.add_argument(
        '--num_top_predictions',
        type=int,
        default=5,
        help='Display this many predictions.'
    )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
